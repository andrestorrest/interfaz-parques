package src.App.Services.GraphicServices;
import java.awt.Color;
import java.awt.Cursor;
import javax.swing.ImageIcon;



public class RecursosService {
    private static RecursosService servicio;
    private Color colorPrincipal,colorAzul;
    private Cursor cursorMano;
    private ImageIcon iFondo,iMadera,iDados,iJ1,iJ2,iJ3,iJ4;
    private ImageIcon cerrar,cerrar1;
    private ImageIcon FR,FV,FAZ,FA;
    private RecursosService(){
        crearColores();
        crearCursor();
        crearImagenes();
    }
    public void crearColores(){
        //color
        colorPrincipal= new Color(0,0,0,210);
        colorAzul= new Color(0,113,221);
    }
    public void crearCursor(){
        //cursor
        cursorMano= new Cursor(Cursor.HAND_CURSOR);
    }
    public void crearImagenes(){
        //imagenes
        iFondo= new ImageIcon("Recursos/Images/Parques.png");
        iMadera= new ImageIcon("Recursos/Images/madera.png");
        iDados= new ImageIcon("Recursos/Images/dados.png");
        iJ1= new ImageIcon("Recursos/Images/j1.png");
        iJ2= new ImageIcon("Recursos/Images/j2.png");
        iJ3= new ImageIcon("Recursos/Images/j3.png");
        iJ4= new ImageIcon("Recursos/Images/j4.png");
        FR= new ImageIcon("Recursos/Images/FR.png");
        FV= new ImageIcon("Recursos/Images/FV.png");
        FA= new ImageIcon("Recursos/Images/FA.png");
        FAZ= new ImageIcon("Recursos/Images/FAZ.png");
        cerrar= new ImageIcon("Recursos/Images/cerrar.png");
        cerrar1= new ImageIcon("Recursos/Images/cerrar1.png");
        
    }

    //getters
    public Color getColorPrincipal(){return colorPrincipal;} 
    public Color getColorAzul(){return colorAzul;}
    public Cursor getCursor(){return cursorMano;}
    public ImageIcon getImagenFondo(){return  iFondo;}
    public ImageIcon getImagenMadera(){return  iMadera;}
    public ImageIcon getImagenDados(){return  iDados;}
    public ImageIcon getImagenJugador1(){return  iJ1;}
    public ImageIcon getImagenJugador2(){return  iJ2;}
    public ImageIcon getImagenJugador3(){return  iJ3;}
    public ImageIcon getImagenJugador4(){return  iJ4;}
    public ImageIcon getImagenFichaRoja(){return  FR;}
    public ImageIcon getImagenFichaVerde(){return  FV;}
    public ImageIcon getImagenFichaAmarilla(){return  FA;}
    public ImageIcon getImagenFichaAzul(){return  FAZ;}
    public ImageIcon getImagenCerrar(){return cerrar;}
    public ImageIcon getImagenCerrar1(){return cerrar1;}

    public static RecursosService getService(){
        if (servicio==null){
            servicio= new RecursosService();
        }
        return servicio;
    }
}
