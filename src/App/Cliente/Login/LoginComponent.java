package src.App.Cliente.Login;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;


import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;


public class LoginComponent extends MouseAdapter implements ActionListener{
    private LoginTemplate loginTemplate;
    
    public LoginComponent(){
        loginTemplate= new LoginTemplate(this);
        
    }
    public LoginTemplate getLoginTemplate(){
        return loginTemplate;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(loginTemplate.getMoverF1())) {
            loginTemplate.getFicha1().setLocation(450, 460);
        }
        if (e.getSource().equals(loginTemplate.getMoverF2())) {
            loginTemplate.getFicha2().setLocation(485, 460);
        }
        if (e.getSource().equals(loginTemplate.getMoverF3())) {
            loginTemplate.getFicha3().setLocation(520, 460);
        }
        if (e.getSource().equals(loginTemplate.getMoverF4())) {
            loginTemplate.getFicha4().setLocation(555, 460);
        }
        if (e.getSource().equals(loginTemplate.getCerrar())) {
            System.exit(0);
        }
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
        
    }
    @Override
    public void mouseEntered(MouseEvent e) {
        if(e.getSource() instanceof JButton){ 
            JButton boton = ((JButton)e.getSource()); 
            boton.setIcon(loginTemplate.getIBlanca(boton));
            }
    }
    @Override
    public void mouseExited(MouseEvent e) {
        if(e.getSource() instanceof JButton){ 
            JButton boton = ((JButton)e.getSource()); 
            boton.setIcon(loginTemplate.getIGris(boton));
            }
        }
    public void restaurarValores(){
        
        
    }

    
    

    
}
