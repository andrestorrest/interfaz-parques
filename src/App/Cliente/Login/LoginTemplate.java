package src.App.Cliente.Login;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import src.App.Services.GraphicServices.ObjGraficService;
import src.App.Services.GraphicServices.RecursosService;


public class LoginTemplate extends JFrame {
    private static final long serialVersionUID = 1L;
    //declaracion de objetos graficos
    private JPanel pIzquierda, pDerecha;
    private JLabel labFondo,labFondo1,labJ1,labJ2,labJ3,labJ4;
    private JLabel FR1,FR2,FR3,FR4;
    private JLabel FV1,FV2,FV3,FV4;
    private JLabel FA1,FA2,FA3,FA4;
    private JLabel FAZ1,FAZ2,FAZ3,FAZ4;
    private JButton bInicio,bDado,bMoverF1,bMoverF2,bMoverF3,bMoverF4,bCerrar;
    private ImageIcon iDim;

    //declaración de servicios
    private ObjGraficService sObjGrafics;
    private RecursosService sRecursos;
    //declaración componente
    private LoginComponent loginComponent;

    public LoginTemplate(LoginComponent loginComponent) {
        sObjGrafics= ObjGraficService.getService();
        sRecursos= RecursosService.getService();
        this.loginComponent= loginComponent;
        //ventana
        setTitle("login de usuario");
        setSize(1200, 600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(this);
        setUndecorated(true);
        setLayout(null);
        setVisible(true);
        //funciones
        crearJPanels();
        crearJButton();
        crearJLabels();
    }
    public void crearJPanels(){
        //panel1
        pDerecha= sObjGrafics.construirPanel(1200, 600, 0,0, null);
       
       //panel 2
        pIzquierda= sObjGrafics.construirPanel(800, 600, 200,0, null);
        this.add(pIzquierda);
        this.add(pDerecha);
    }

    public void crearJLabels(){
        //fichas
        //redimencionar Fichas Rojas
        iDim= new ImageIcon(sRecursos.getImagenFichaRoja().getImage().getScaledInstance(30, 30, Image.SCALE_AREA_AVERAGING));
        //Ficha Roja 1
        FR1= sObjGrafics.construirJLabel("", 23, 145, 30, 30, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(FR1);
        //Ficha Roja 2
        FR2= sObjGrafics.construirJLabel("", 80, 145, 30, 30, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(FR2);
        //Ficha Roja 3
        FR3= sObjGrafics.construirJLabel("", 140, 145, 30, 30, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(FR3);
        //Ficha Roja 4
        FR4= sObjGrafics.construirJLabel("", 200, 145, 30, 30, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(FR4);

        //redimencionar Fichas verdes
        iDim= new ImageIcon(sRecursos.getImagenFichaVerde().getImage().getScaledInstance(30, 30, Image.SCALE_AREA_AVERAGING));
        //Ficha Verde 1
        FV1= sObjGrafics.construirJLabel("", 23, 445, 30, 30, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(FV1);
        //Ficha Verde 2
        FV2= sObjGrafics.construirJLabel("", 80, 445, 30, 30, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(FV2);
        //Ficha Verde 3
        FV3= sObjGrafics.construirJLabel("", 140, 445, 30, 30, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(FV3);
        //Ficha Verde 4
        FV4= sObjGrafics.construirJLabel("", 200, 445, 30, 30, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(FV4);

        //redimencionar Fichas Amarillas
        iDim= new ImageIcon(sRecursos.getImagenFichaAmarilla().getImage().getScaledInstance(30, 30, Image.SCALE_AREA_AVERAGING));
        //Ficha Amarilla 1
        FA1= sObjGrafics.construirJLabel("", 575, 445, 30, 30, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(FA1);
        //Ficha Amarilla 2
        FA2= sObjGrafics.construirJLabel("", 635, 445, 30, 30, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(FA2);
        //Ficha Amarilla 3
        FA3= sObjGrafics.construirJLabel("", 693, 445, 30, 30, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(FA3);
        //Ficha Amarilla 4
        FA4= sObjGrafics.construirJLabel("", 750, 445, 30, 30, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(FA4);

        //redimencionar Fichas Azules
        iDim= new ImageIcon(sRecursos.getImagenFichaAzul().getImage().getScaledInstance(30, 30, Image.SCALE_AREA_AVERAGING));
        //Ficha Azul 1
        FAZ1= sObjGrafics.construirJLabel("", 574, 145, 30, 30, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(FAZ1);
        //Ficha Azul 2
        FAZ2= sObjGrafics.construirJLabel("", 633, 145, 30, 30, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(FAZ2);
        //Ficha Azul 3
        FAZ3= sObjGrafics.construirJLabel("", 690, 145, 30, 30, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(FAZ3);
        //Ficha Azul 4
        FAZ4= sObjGrafics.construirJLabel("", 750, 145, 30, 30, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(FAZ4);


        //Jugadores
        //redimencionar jugador Rojo
        iDim= new ImageIcon(sRecursos.getImagenJugador3().getImage().getScaledInstance(45, 45, Image.SCALE_AREA_AVERAGING));
        //Jugador Rojo
        labJ1= sObjGrafics.construirJLabel("", 100, 71, 45, 45, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(labJ1);
        //redimencionar jugador Verde
        iDim= new ImageIcon(sRecursos.getImagenJugador2().getImage().getScaledInstance(45, 45, Image.SCALE_AREA_AVERAGING));
        //Jugador Verde
        labJ2= sObjGrafics.construirJLabel("", 100, 483, 45, 45, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(labJ2);
        //redimencionar jugador Azul
        iDim= new ImageIcon(sRecursos.getImagenJugador1().getImage().getScaledInstance(45, 45, Image.SCALE_AREA_AVERAGING));
        //Jugador Azul
        labJ3= sObjGrafics.construirJLabel("", 652, 71, 45, 45, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(labJ3);
        //redimencionar jugador Amarillo
        iDim= new ImageIcon(sRecursos.getImagenJugador4().getImage().getScaledInstance(45, 45, Image.SCALE_AREA_AVERAGING));
        //Jugador Amarillo
        labJ4= sObjGrafics.construirJLabel("", 652, 483, 45, 45, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(labJ4);



        //Fondos
        //redimencionar fondo
        iDim= new ImageIcon(sRecursos.getImagenMadera().getImage().getScaledInstance(1200, 600, Image.SCALE_AREA_AVERAGING));
        //fondo
        labFondo1= sObjGrafics.construirJLabel("", 0, 0, 1200, 600, 
        null, iDim, null, null, null, null, "");
        pDerecha.add(labFondo1);
        //redimencionar Tablero
        iDim= new ImageIcon(sRecursos.getImagenFondo().getImage().getScaledInstance(800, 600, Image.SCALE_AREA_AVERAGING));
        //Tablero
        labFondo= sObjGrafics.construirJLabel("", 0, 0, 800, 600, 
        null, iDim, null, null, null, null, "");
        pIzquierda.add(labFondo);
    }
    public void crearJButton(){
        //redimencionar fondo
        iDim= new ImageIcon(sRecursos.getImagenDados().getImage().getScaledInstance(75, 110, Image.SCALE_AREA_AVERAGING));
        //boton dado
        bDado= sObjGrafics.construirJButton("", 1050,400,75, 110, 
        sRecursos.getCursor(), iDim, null, null, null, null, "", true);
        bDado.addActionListener(loginComponent);
       // bDado.addMouseListener(loginComponent);
        pDerecha.add(bDado);
        //boton mover ficha 1
        bMoverF1= sObjGrafics.construirJButton("Mover ficha 1", 1020,150,140, 40, 
        sRecursos.getCursor(), null, null, null, null, null, "", true);
        bMoverF1.addActionListener(loginComponent);
       // bMover.addMouseListener(loginComponent);
        pDerecha.add(bMoverF1);
         //boton mover ficha 2
         bMoverF2= sObjGrafics.construirJButton("Mover ficha 2", 1020,200,140, 40, 
         sRecursos.getCursor(), null, null, null, null, null, "", true);
         bMoverF2.addActionListener(loginComponent);
        // bMover.addMouseListener(loginComponent);
         pDerecha.add(bMoverF2);
          //boton mover ficha 3
        bMoverF3= sObjGrafics.construirJButton("Mover ficha 3", 1020,250,140, 40, 
        sRecursos.getCursor(), null, null, null, null, null, "", true);
        bMoverF3.addActionListener(loginComponent);
       // bMover.addMouseListener(loginComponent);
        pDerecha.add(bMoverF3);
         //boton mover ficha 4
         bMoverF4= sObjGrafics.construirJButton("Mover ficha 4", 1020,300,140, 40, 
         sRecursos.getCursor(), null, null, null, null, null, "", true);
         bMoverF4.addActionListener(loginComponent);
        // bMover.addMouseListener(loginComponent);
         pDerecha.add(bMoverF4);
        //redimencionar boton
        iDim= new ImageIcon(sRecursos.getImagenCerrar().getImage().getScaledInstance(40, 40, Image.SCALE_AREA_AVERAGING));
        //boton cerrar
        bCerrar= sObjGrafics.construirJButton("", 1150,10,40, 40, 
        sRecursos.getCursor(), iDim, null, null, null, null, "", false);
        bCerrar.addActionListener(loginComponent);
        bCerrar.addMouseListener(loginComponent);
        pDerecha.add(bCerrar);
    }
    public ImageIcon getIBlanca(JButton boton){
        if(boton==bCerrar){
            iDim= new ImageIcon(sRecursos.getImagenCerrar1().getImage().getScaledInstance(40, 40, Image.SCALE_AREA_AVERAGING));
        }
        return iDim;
    }
    public ImageIcon getIGris(JButton boton){
        if(boton==bCerrar){
            iDim= new ImageIcon(sRecursos.getImagenCerrar().getImage().getScaledInstance(40, 40, Image.SCALE_AREA_AVERAGING));
        }
        return iDim;
    }
    public JButton getInicio(){return bInicio;}
    public JButton getCerrar(){return bCerrar;}
    public JButton getMoverF1(){return bMoverF1;}
    public JButton getMoverF2(){return bMoverF2;}
    public JButton getMoverF3(){return bMoverF3;}
    public JButton getMoverF4(){return bMoverF4;}
    public JLabel getFicha1(){return FA1;}
    public JLabel getFicha2(){return FA2;}
    public JLabel getFicha3(){return FA3;}
    public JLabel getFicha4(){return FA4;}
}